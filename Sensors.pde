void motionCalibrate() {
    lcdPrintStr(1,1,"Kalibruojama: ");
    
    for(int i = 0; i < 30; i++){
		int rem =  30 - i;
		lcdPrintInt(1,15, rem);		
		delay(1000);
    }
    lcdClear();
    lcdPrintStr(1,1, "Kalibracija");
	lcdPrintStr(2,1, "BAIGTA.");
	delay(2000);
	lcdClear();
}
/*
void motionActive(boolean enableBeep) {  
  if(digitalRead(MotionPin) == HIGH){
    lcdClear();
    lcdPrintStr(1,1,"PAZEISTA ZONA");
            
    if(enableBeep) {
      digitalWrite(BeepPin, HIGH);
      delay(100);
      digitalWrite(BeepPin, LOW);
      delay(900);
    } else {
      delay(1000);
    }
  }
}
*/
