void lcdPrintStr(int row, int pos, char msg[16]) {	
	Serial.print("$GO ");
	Serial.print(row);
	Serial.print(" ");
	Serial.print(pos);
	Serial.print("\r\n");
	
	Serial.print("$PRINT ");
	Serial.print(msg);
	Serial.print("\r\n");
}

void lcdPrintInt(int row, int pos, int msg) {	
	Serial.print("$GO ");
	Serial.print(row);
	Serial.print(" ");
	Serial.print(pos);
	Serial.print("\r\n");
	
	Serial.print("$PRINT ");
	Serial.print(msg);
	Serial.print("\r\n");
}

void lcdClear() {
	Serial.print("$CLEAR\r\n");
}

void lcdHome() {
	Serial.print("$HOME\r\n");
}

void lcdCursor(int show, int blink) {
	Serial.print("$CURSOR ");
	Serial.print(show);
	Serial.print(" ");
	Serial.print(blink);
	Serial.print("\r\n");	
}
