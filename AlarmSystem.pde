#include <Keypad.h>

/* Keyboard */
const byte myRows = 4;  // number of rows
const byte myCols = 3;  //number of columns vf

char keys[myRows][myCols] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
}; 
byte rowPins[myRows] = {9, 8, 7, 6 }; //array to map keypad to MCU pins
byte colPins[myCols] = {12, 11, 10 }; //array to map keypad to MCU pins
Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, myRows, myCols ); //keypad library map function

/* Sensors */
const int MotionPin = 5;

/* Indicators */
const int BeepPin =  4;     

/* Settings */
char inputKeys[5]; 
char password[5] = {'1','2','3','4', '#'}; 

boolean alarmBreaked = false;
boolean alarmActivated = false;

int index = 0;
boolean alarmSystem = false;


/* Functions */
void setup() {
  Serial.begin(9600);
  lcdClear();
  
  pinMode(BeepPin, OUTPUT);
  motionCalibrate();
  
  lcdPrintStr(1,1,"Signalizacija");
  lcdPrintStr(2,1,"isjungta");
}


void loop() {
  if(alarmActivated) {
    if(alarmBreaked) {
      //indicate that alarm was braked
    } else {
      if(digitalRead(MotionPin) == HIGH) {
        alarmStart(1000);
        alarmBreaked = true;
      }
    }    
  }
  
  
  char key = kpd.getKey();
  
  if(key) {
    // checking password
    if(key == password[index]) {
      index++;
      Serial.print(key);
    } else {
      index = 0;
      Serial.println(key);
    }
    
    // if user entered password correctly
    if(index == 5) {
      if(alarmActivated) {
        
        alarmActivated = false;
        lcdClear();
        lcdPrintStr(1,1,"Signalizacija");
        lcdPrintStr(2,1,"isjungta");
      } else {
        alarmActivated = true;
        alarmBreaked = false;
        lcdClear();
        lcdPrintStr(1,1,"Signalizacija");
        lcdPrintStr(2,1,"aktyvuota");
        delay(2000);
      }
      //beep(1000);
      index = 0;
      Serial.println(' ');
    }
  } 
  
}
  
